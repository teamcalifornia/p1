# theREADME #

Hey all, thought I'd clean our projects up a little bit and keep it a little more organized. Also wanted to integrate HipChat to our projects to help us with communication.

### GIT QUICK START GUIDE ###

* Malik emailed everyone a GIT guide which can be found [here](http://www.sbf5.com/~cduan/technical/git/)
* git clone https://mandocdoc@bitbucket.org/teamcalifornia/p1.git
* git remote add p1 https://mandocdoc@bitbucket.org/teamcalifornia/p1.git
* git pull p1 master
* git commit -a -m "REASON FOR COMMIT"
* git push p1

### QUICK MARKDOWN TIPS ###

* \\ To escape
* \#\#\# To bold \#\#\#
* \* To make a list
* \[Link Title\]\(Link URL\)